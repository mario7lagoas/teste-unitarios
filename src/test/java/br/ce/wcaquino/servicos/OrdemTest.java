package br.ce.wcaquino.servicos;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrdemTest {
	
	private static int contator = 0;
	
	@Test
	public void inicia() {
		contator++;
	}
	
	@Test
	public void verfica() {
		Assert.assertEquals(1, contator);
	}

}
