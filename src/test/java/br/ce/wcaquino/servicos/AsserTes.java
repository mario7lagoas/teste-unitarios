package br.ce.wcaquino.servicos;

import org.junit.Assert;
import org.junit.Test;

import br.ce.wcaquino.entidades.Usuario;

public class AsserTes {
	@Test
	public void test() {
		Assert.assertTrue(true);
		Assert.assertFalse(false);
		
		Assert.assertEquals("Erro de Compara��o", 1, 1);
		
		Assert.assertEquals(0.51234, 0.512, 0.001);
		Assert.assertEquals(Math.PI, 3.14, 0.01);
		
		int i = 5;
		Integer i2 = 5;
		Assert.assertEquals(Integer.valueOf(i), i2);
		Assert.assertEquals(i, i2.intValue());
		
		Assert.assertEquals("bola", "bola");
		Assert.assertNotEquals("bola", "casa");
		Assert.assertTrue("bola".equalsIgnoreCase("Bola") );
		Assert.assertTrue("bola".startsWith("bo") );
		
		Usuario u1 = new Usuario("Usuario 1");
		Usuario u2 = new Usuario("Usuario 1");
		Usuario u3 = null;
		
		Assert.assertEquals(u1, u2); //Gerar hashCode and Equals -> verificar se s�o iguais
		Assert.assertSame(u2, u2); // verificar se s�o da mesma instacia
		Assert.assertNotSame(u1, u2); // verificar se  n�o s�o da mesma instacia
		Assert.assertNull(u3); //verificar se o objeto � null
		Assert.assertNotNull(u2); //verificar se o objeto  n�o� null
	}

}
