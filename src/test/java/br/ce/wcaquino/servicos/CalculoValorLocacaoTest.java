package br.ce.wcaquino.servicos;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.ce.wcaquino.daos.LocacaoDAO;
import br.ce.wcaquino.daos.LocacaoDAOFake;
import br.ce.wcaquino.entidades.Filme;
import br.ce.wcaquino.entidades.Locacao;
import br.ce.wcaquino.entidades.Usuario;
import br.ce.wcaquino.exceptions.FilmeSemEsoqueExeption;
import br.ce.wcaquino.exceptions.LocadoraException;

@RunWith(Parameterized.class)
public class CalculoValorLocacaoTest {
	
	@Parameter
	public List<Filme> filmes;
	
	@Parameter(value=1)
	public Double valorLocacao;

	@Parameter(value=2)
	public String cenario;

	@InjectMocks
	private LocacaoService locacaoService;
	
	@Mock
	private LocacaoDAO dao;
	
	@Mock
	private SPCService spc;
	
	private static Filme filme1 = new Filme("Filme 1", 2, 4.0);
	private static Filme filme2 = new Filme("Filme 2", 2, 4.0);
	private static Filme filme3 = new Filme("Filme 3", 2, 4.0);
	private static Filme filme4 = new Filme("Filme 4", 2, 4.0);
	private static Filme filme5 = new Filme("Filme 5", 2, 4.0);
	private static Filme filme6 = new Filme("Filme 6", 2, 4.0);
	private static Filme filme7 = new Filme("Filme 4", 2, 4.0);
	
	@Before
	public void setup() {
		
		MockitoAnnotations.initMocks(this);
		/*
		 * Com o Mockito n�o precisamos mais 
		 * dessas intancias abaixo
		 * 
		 locacaoService = new LocacaoService();

		 LocacaoDAO dao = Mockito.mock(LocacaoDAO.class);
		 locacaoService.setLocacaoDAO(dao);
		 
		 SPCService spc = Mockito.mock(SPCService.class);
		 locacaoService.setSPCService(spc);
		 */
	}
	
	@Parameters(name= "Teste {index} =  {2}")
	public static Collection<Object[]> getParementros(){
		return Arrays.asList(new Object[][] {
			{Arrays.asList(filme1, filme2),8.0, "2 Filmes: Sem desconto"},
			{Arrays.asList(filme1, filme2, filme3),11.0, "3 Filmes: 25%"},
			{Arrays.asList(filme1, filme2, filme3, filme4),13.0, "4 Filmes: 50%"},
			{Arrays.asList(filme1, filme2, filme3, filme4, filme5),14.0, "5 Filmes: 75%"},
			{Arrays.asList(filme1, filme2, filme3, filme4, filme5, filme6 ),14.0, "6 Filmes: 100%"},
			{Arrays.asList(filme1, filme2, filme3, filme4, filme5, filme6, filme7 ),18.0, "7 Filmes: Sem desconto"}
		});
		
	}

	@Test
	public void deveCalcularValorLocacaoConsiderandoDescontos() throws FilmeSemEsoqueExeption, LocadoraException {
		//cenario
		Usuario usuario = new Usuario("Usuario 1");

		
		//a��o
		
		Locacao result = locacaoService.alugarFilme(usuario, filmes);
		
		//verifica��o
		
		assertThat(result.getValor(), is(valorLocacao));
		
	}	
}
