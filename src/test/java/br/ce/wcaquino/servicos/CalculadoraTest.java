package br.ce.wcaquino.servicos;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import br.ce.wcaquino.exceptions.NaopodeDividirPorZeroException;
import br.ce.wcaquino.runners.ParallelRunner;

//@RunWith(ParallelRunner.class)
public class CalculadoraTest {
	
	private Calculadora calc;
	
	@Before
	public void setup() {
		calc = new Calculadora();
	}
	
	@Test
	public void deveSomarDoisValores() {
		//cenario
		int a = 5;
		int b = 3;
		
//		Calculadora calc = new Calculadora();
		
		//a��o
		
		int result = calc.somar(a,b);
		
		//verifica��o
		Assert.assertEquals(8, result);
	}
	
	@Test
	public void deveSubtrairDoisValores() {
		//cenario
		int a = 8;
		int b = 5;
		
		//a��o
		int result = calc.subtrair(a, b);
		
		//verifica��o
		Assert.assertEquals(3, result);
		
	}
	
	@Test
	public void deveDividirDoisValores() throws NaopodeDividirPorZeroException {
		//cenario
		int a = 6;
		int b = 3;
		
		//a��o
		
		int result = calc.dividir(a, b);
		
		//verifica��o
		Assert.assertEquals(2, result);
		
	}
	
	@Test(expected = NaopodeDividirPorZeroException.class)
	public void deveLancarExecaoAoDividirPorZero() throws NaopodeDividirPorZeroException {
		//cenario
		int a = 10;
		int b = 0;
		
		//a��o
		calc.dividir(a, b);
		
	}

}
