package br.ce.wcaquino.servicos;

import static br.ce.wcaquino.builders.FilmeBuilder.umFilme;
import static br.ce.wcaquino.builders.LocacaoBuilder.umaLocacao;
import static br.ce.wcaquino.builders.UsuarioBuilder.umUsuario;
import static br.ce.wcaquino.matchers.MatchersProprios.caiEm;
import static br.ce.wcaquino.matchers.MatchersProprios.caiNumaSegunda;
import static br.ce.wcaquino.matchers.MatchersProprios.ehHoje;
import static br.ce.wcaquino.matchers.MatchersProprios.ehHojeComDiferencaDias;
import static br.ce.wcaquino.utils.DataUtils.isMesmaData; //imports estacticos
import static br.ce.wcaquino.utils.DataUtils.obterDataComDiferencaDias; //imports estacticos

//import estatico no eclipse � ctrl + shift + M

import static org.hamcrest.CoreMatchers.equalTo; //imports estacticos
import static org.hamcrest.CoreMatchers.is; //imports estacticos
import static org.hamcrest.CoreMatchers.not; //imports estacticos
import static org.junit.Assert.assertEquals; //imports estacticos
import static org.junit.Assert.assertThat; //imports estacticos
import static org.junit.Assert.assertTrue; //imports estacticos

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Rule;
import org.junit.After;
import org.junit.AfterClass;
//import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ce.wcaquino.builders.FilmeBuilder;
import br.ce.wcaquino.builders.LocacaoBuilder;
import br.ce.wcaquino.builders.UsuarioBuilder;
import br.ce.wcaquino.daos.LocacaoDAO;
import br.ce.wcaquino.daos.LocacaoDAOFake;
import br.ce.wcaquino.entidades.Filme;
import br.ce.wcaquino.entidades.Locacao;
import br.ce.wcaquino.entidades.Usuario;
import br.ce.wcaquino.exceptions.FilmeSemEsoqueExeption;
import br.ce.wcaquino.exceptions.LocadoraException;
import br.ce.wcaquino.matchers.DiaSemanaMatchers;
import br.ce.wcaquino.matchers.MatchersProprios;
import br.ce.wcaquino.servicos.LocacaoService;
import br.ce.wcaquino.utils.DataUtils;

public class LocacaoServiceTest {

	@InjectMocks @Spy
	private LocacaoService locacaoService;
	
	@Mock
	private SPCService spc;
	@Mock
	private LocacaoDAO dao;
	@Mock
	private EmailService email;

	@Rule
	public ErrorCollector error = new ErrorCollector();

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setup() {
		
		MockitoAnnotations.initMocks(this);
	
		/*
		 * Com o Mockito n�o precisamos mais 
		 * dessas intancias abaixo
		 * 
		locacaoService = new LocacaoService();

		dao = Mockito.mock(LocacaoDAO.class);
		locacaoService.setLocacaoDAO(dao);

		spc = Mockito.mock(SPCService.class);
		locacaoService.setSPCService(spc);
		
		email = Mockito.mock(EmailService.class);
		locacaoService.setEmailService(email);
		*/
	}
	/*
	 * @After public void tearDown() { System.out.println("After"); }
	 * 
	 * @BeforeClass public static void setupClass() {
	 * System.out.println("Before Class");
	 * 
	 * }
	 * 
	 * @AfterClass public static void tearDownClass() {
	 * System.out.println("After Class"); }
	 */

	@Test
	public void deveAlugarFilme() throws Exception {
		// cenario

		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().comValor(5.0).agora());
		
		Mockito.doReturn(DataUtils.obterData(7, 1, 2022)).when(locacaoService).obterData();

		// a��o
		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o
		error.checkThat(locacao.getValor(), is(5.0));
		error.checkThat(isMesmaData(locacao.getDataLocacao(), DataUtils.obterData(7, 1, 2022)), is(true));
		error.checkThat(isMesmaData((locacao.getDataRetorno()), DataUtils.obterData(8, 1, 2022)),is(true));

	}

	// forma elegante

	@Test(expected = FilmeSemEsoqueExeption.class) // Espera que seja lan�ada uma exce��o
	public void deveLancharExcecaoAoAlugarFilmeSemEstoque() throws Exception {
		// cenario

		// LocacaoService locacaoService = new LocacaoService();
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().semEstoque().agora());

		// a��o
		locacaoService.alugarFilme(usuario, filmes);

	}
	/*
	 * 
	 * //forma robusta
	 * 
	 * @Test public void testLocacao_filmeSemEstoque_2() { //cenario
	 * 
	 * LocacaoService locacaoService = new LocacaoService(); Usuario usuario = new
	 * Usuario("Usuario 1"); Filme filme = new Filme("Filme 1", 0, 5.0);
	 * 
	 * 
	 * //a��o try { locacaoService.alugarFilme(usuario, filme);
	 * Assert.fail("Deveria ter lan�ando uma exce��o"); } catch (Exception e) {
	 * 
	 * assertThat(e.getMessage(), is("Filme sem estoque.")); }
	 * 
	 * }
	 * 
	 * @Test public void testLocacao_filmeSemEstoque_3() throws Exception {
	 * //cenario
	 * 
	 * LocacaoService locacaoService = new LocacaoService(); Usuario usuario = new
	 * Usuario("Usuario 1"); Filme filme = new Filme("Filme 1", 0, 5.0);
	 * 
	 * exception.expect(Exception.class);
	 * exception.expectMessage("Filme sem estoque.");
	 * 
	 * 
	 * //a��o
	 * 
	 * locacaoService.alugarFilme(usuario, filme);
	 * 
	 * }
	 */

	@Test
	public void naoDeveAlugarFilmeSemUsuario() throws FilmeSemEsoqueExeption {
		// cenario
		// LocacaoService locacaoService = new LocacaoService();
		List<Filme> filmes = Arrays.asList(umFilme().agora());

		// a��o
		try {
			locacaoService.alugarFilme(null, filmes);
			Assert.fail();
		} catch (LocadoraException e) {
			Assert.assertThat(e.getMessage(), is("Usuario Vazio."));
		}

	}

	@Test
	public void naoDeveAlugarFilmeComSemEstoque() throws FilmeSemEsoqueExeption, LocadoraException {
		// cenario
		// LocacaoService locacaoService = new LocacaoService();
		Usuario usuario = umUsuario().agora();

		exception.expect(LocadoraException.class);
		exception.expectMessage("Filme Vazio.");

		// a��o
		locacaoService.alugarFilme(usuario, null);

	}

	@Test
	public void devePagar75PorcentoNoFilme3() throws FilmeSemEsoqueExeption, LocadoraException {
		// cenario
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().agora(), new Filme("Filme 2", 2, 4.0),
				new Filme("Filme 3", 2, 4.0));

		// a��o

		Locacao result = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o

		assertThat(result.getValor(), is(11.0));

	}

	@Test
	public void devePagar50PorcentoNoFilme4() throws FilmeSemEsoqueExeption, LocadoraException {
		// cenario
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(new Filme("Filme 1", 2, 4.0), new Filme("Filme 2", 2, 4.0),
				new Filme("Filme 3", 2, 4.0), new Filme("Filme 4", 2, 4.0));

		// a��o

		Locacao result = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o

		assertThat(result.getValor(), is(13.0));

	}

	@Test
	public void devePagar25PorcentoNoFilme5() throws FilmeSemEsoqueExeption, LocadoraException {
		// cenario
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(new Filme("Filme 1", 2, 4.0), new Filme("Filme 2", 2, 4.0),
				new Filme("Filme 3", 2, 4.0), new Filme("Filme 4", 2, 4.0), new Filme("Filme 5", 2, 4.0));

		// a��o

		Locacao result = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o

		assertThat(result.getValor(), is(14.0));

	}

	@Test
	public void devePagarZeroFilme6() throws FilmeSemEsoqueExeption, LocadoraException {
		// cenario
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(new Filme("Filme 1", 2, 4.0), new Filme("Filme 2", 2, 4.0),
				new Filme("Filme 3", 2, 4.0), new Filme("Filme 4", 2, 4.0), new Filme("Filme 5", 2, 4.0),
				new Filme("Filme 6", 2, 4.0));

		// a��o

		Locacao result = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o

		assertThat(result.getValor(), is(14.0));

	}

	@Test
	public void deveDevolverNaSegundaAoAlugarNoSabado() throws Exception {

		// cenario
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().agora());
		
		Mockito.doReturn(DataUtils.obterData(8, 1, 2022)).when(locacaoService).obterData();
		
		// a��o
		Locacao result = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o
		assertThat(result.getDataRetorno(), caiNumaSegunda());

	}

	@Test
	public void naoDeveAlugarFilmeParaNegativadoSpc() throws Exception {
		// cen�rio
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().agora());

		Mockito.when(spc.possuiNegativacao(usuario)).thenReturn(true);
		// usando mockito para um usuario generico
		/*
		Mockito.when(spc.possuiNegativacao(Mockito.any(Usuario.class))).thenReturn(true);
		*/
		


		// a��o
		try {
			locacaoService.alugarFilme(usuario, filmes);
			//verificacao
			Assert.fail();
		} catch (LocadoraException e) {
			Assert.assertThat(e.getMessage(), is("Usuario Negativado."));
		}
		
		Mockito.verify(spc).possuiNegativacao(usuario);

	}

	@Test
	public void deveEnviarEmailParaLocacoesAtrasadas() {
		// cenario
		Usuario usuario = umUsuario().agora();
		Usuario usuario2 = umUsuario().comNome("Usuario em dia").agora();
		Usuario usuario3 = umUsuario().comNome("Outro atraso").agora();
			
		List<Locacao> locacoes = Arrays.asList(umaLocacao()
				.comUsuario(usuario).atrasado().agora(),
				umaLocacao().comUsuario(usuario2).agora(),
				umaLocacao().atrasado().comUsuario(usuario3).agora(),
				umaLocacao().atrasado().comUsuario(usuario3).agora());
		
		Mockito.when(dao.obterLocacoesPendentes()).thenReturn(locacoes);

		// a��o
		locacaoService.notificarAtrasos();
		
		//verifica��o
		Mockito.verify(email, Mockito.times(3)).notificarAtraso(Mockito.any(Usuario.class));
		
		Mockito.verify(email).notificarAtraso(usuario);
		Mockito.verify(email, Mockito.times(2)).notificarAtraso(usuario3);
		Mockito.verify(email, Mockito.never()).notificarAtraso(usuario2);
		Mockito.verifyNoMoreInteractions(email);

	}

	@Test
	public void deveTratarErroNoSPC() throws Exception {
		//cen�rio
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().agora());
		
		Mockito.when(spc.possuiNegativacao(usuario)).thenThrow(new Exception("Falha catratr�fica."));
		
		//verifica��o
		exception.expect(LocadoraException.class);
		exception.expectMessage("Problemas com SPC, tente novamente.");
		
		//a��o
		locacaoService.alugarFilme(usuario, filmes);
		
	}
	
	@Test
	public void deveProrrogarUmaLocacao() {
		//cenario
		Locacao locacao = umaLocacao().agora();
		
		
		//acao
		locacaoService.prorrogarLocacao(locacao, 3);
		
		//verificacao
		ArgumentCaptor<Locacao> argCapt = ArgumentCaptor.forClass(Locacao.class);
		Mockito.verify(dao).salvar(argCapt.capture());
		Locacao locacaoRetornada = argCapt.getValue();
		
		error.checkThat(locacaoRetornada.getValor(),is(12.0));
		error.checkThat(locacaoRetornada.getDataLocacao(),ehHoje());
		error.checkThat(locacaoRetornada.getDataRetorno(),ehHojeComDiferencaDias(3));
		
	}

	
	@Test
	public void deveCalcularValorLocacao() throws Exception {
		//cenario
		List<Filme> filmes = Arrays.asList(umFilme().agora());
		
		//a��o
		Class<LocacaoService> clazz = LocacaoService.class;
		Method metodo = clazz.getDeclaredMethod("calcularValorLocacao", List.class);
		metodo.setAccessible(true);
		Double valor = (Double) metodo.invoke(locacaoService, filmes);
	
		/*
	 * retirado o poswerMock	
	 
		Double valor = (Double) Whitebox.invokeMethod(locacaoService, "calcularValorLocacao", filmes);
	*/
		
		//verifica��o
		Assert.assertThat(valor, is(4.0));
	
	}
}
