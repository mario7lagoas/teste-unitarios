package br.ce.wcaquino.servicos;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;



public class CalculadoraMockTest {
	
	@Mock
	private Calculadora calcMok;
	
	@Spy
	private Calculadora calcSpy;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
	}
	
	@Test
	public void deveMostrarDiferencaEntreMockSpy() {
		Mockito.when(calcMok.somar(1, 2)).thenReturn(8);
		//Tambem � possivel o mok trazer o valor real
		// Mockito.when(calcMok.somar(1, 2)).thenCallRealMethod();
		
		//Mockito.when(calcSpy.somar(1, 2)).thenReturn(8);
		
		//Usar spy assim para evitar erro e interpreta��o do java
		Mockito.doReturn(8).when(calcSpy).somar(1, 2);
	
		/*
		System.out.println("Mock :" + calcMok.somar(1, 2));
		System.out.println("Spy  :" + calcSpy.somar(1, 2));
	
	
		 * O Mock quando n�o sabe o que fazer devolve Zero
		 * O Spy quando n�o sabe o que fazer devolver o valor real do metodo
		 * 
		 * O Spy n�o pode ser usuado em interface
		 */
	}
	
	@Test
	public void teste() {
		Calculadora calc = Mockito.mock(Calculadora.class);
		
		Mockito.when(calc.somar(1, 2)).thenReturn(5);
		//usando matchers
		Mockito.when(calc.somar(Mockito.eq(1), Mockito.anyInt())).thenReturn(5);
		
		Assert.assertEquals(5, calc.somar(1, 2));
		
	}

}
