package br.ce.wcaquino.servicos;

import static br.ce.wcaquino.builders.FilmeBuilder.umFilme;
import static br.ce.wcaquino.builders.LocacaoBuilder.umaLocacao;
import static br.ce.wcaquino.builders.UsuarioBuilder.umUsuario;
import static br.ce.wcaquino.matchers.MatchersProprios.caiEm;
import static br.ce.wcaquino.matchers.MatchersProprios.caiNumaSegunda;
import static br.ce.wcaquino.matchers.MatchersProprios.ehHoje;
import static br.ce.wcaquino.matchers.MatchersProprios.ehHojeComDiferencaDias;
import static br.ce.wcaquino.utils.DataUtils.isMesmaData; //imports estacticos
import static br.ce.wcaquino.utils.DataUtils.obterDataComDiferencaDias; //imports estacticos

//import estatico no eclipse � ctrl + shift + M

import static org.hamcrest.CoreMatchers.equalTo; //imports estacticos
import static org.hamcrest.CoreMatchers.is; //imports estacticos
import static org.hamcrest.CoreMatchers.not; //imports estacticos
import static org.junit.Assert.assertEquals; //imports estacticos
import static org.junit.Assert.assertThat; //imports estacticos
import static org.junit.Assert.assertTrue; //imports estacticos

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Rule;
import org.junit.After;
import org.junit.AfterClass;
//import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ce.wcaquino.builders.FilmeBuilder;
import br.ce.wcaquino.builders.LocacaoBuilder;
import br.ce.wcaquino.builders.UsuarioBuilder;
import br.ce.wcaquino.daos.LocacaoDAO;
import br.ce.wcaquino.daos.LocacaoDAOFake;
import br.ce.wcaquino.entidades.Filme;
import br.ce.wcaquino.entidades.Locacao;
import br.ce.wcaquino.entidades.Usuario;
import br.ce.wcaquino.exceptions.FilmeSemEsoqueExeption;
import br.ce.wcaquino.exceptions.LocadoraException;
import br.ce.wcaquino.matchers.DiaSemanaMatchers;
import br.ce.wcaquino.matchers.MatchersProprios;
import br.ce.wcaquino.servicos.LocacaoService;
import br.ce.wcaquino.utils.DataUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocacaoService.class, DataUtils.class})
public class LocacaoServiceTest_PowerMock {

	@InjectMocks
	private LocacaoService locacaoService;
	
	@Mock
	private SPCService spc;
	@Mock
	private LocacaoDAO dao;
	@Mock
	private EmailService email;

	@Rule
	public ErrorCollector error = new ErrorCollector();

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setup() {
		
		MockitoAnnotations.initMocks(this);
		
		locacaoService = PowerMockito.spy(locacaoService);

	}

	@Test
	public void deveAlugarFilme() throws Exception {
		// cenario

		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().comValor(5.0).agora());
		
		PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(DataUtils.obterData(7, 1, 2022));

		// a��o
		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o
		/*
		 * assertEquals(5.0, locacao.getValor(), 0.01); assertThat(locacao.getValor(),
		 * is(5.0));
		 */

		error.checkThat(locacao.getValor(), is(5.0));
		/*
		 * assertThat(locacao.getValor(), is(equalTo(5.0)));
		 * assertThat(locacao.getValor(), is(not(6.0)));
		 */

		error.checkThat(locacao.getValor(), is(not(6.0)));

		/*
		 * assertTrue(isMesmaData(locacao.getDataLocacao(), new Date()));
		 * assertThat(isMesmaData(locacao.getDataLocacao(), new Date()),is(true));
		 */

		error.checkThat(isMesmaData(locacao.getDataLocacao(), new Date()), is(true));

		// ou
		// usando Matecher personalidazado
		error.checkThat(locacao.getDataLocacao(), ehHoje());

		/*
		 * assertTrue(isMesmaData(locacao.getDataRetorno(),obterDataComDiferencaDias(1))
		 * );
		 * assertThat(isMesmaData(locacao.getDataRetorno(),obterDataComDiferencaDias(1))
		 * ,is(true));
		 */

		error.checkThat(isMesmaData(locacao.getDataRetorno(), obterDataComDiferencaDias(1)), is(true));

		// ou
		// usando Matecher personalidazado
		error.checkThat(locacao.getDataRetorno(), ehHojeComDiferencaDias(1));

	}
	@Test
	public void deveDevolverNaSegundaAoAlugarNoSabado() throws Exception {
		
		// cenario
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().agora());

		PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(DataUtils.obterData(8, 1, 2022));
		
		// a��o
		Locacao result = locacaoService.alugarFilme(usuario, filmes);

		// verifica��o
		// boolean ehSegunda = DataUtils.verificarDiaSemana(result.getDataRetorno(),
		// Calendar.MONDAY);
		// Assert.assertTrue(ehSegunda);

		// usando Matecher personalidazado
		assertThat(result.getDataRetorno(), new DiaSemanaMatchers(Calendar.MONDAY));
		// ou
		assertThat(result.getDataRetorno(), caiEm(Calendar.MONDAY));

		assertThat(result.getDataRetorno(), caiNumaSegunda());

	}
	@Test
	public void devealugarFilme_SemCalcularValor() throws Exception {
		//cenario
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(umFilme().agora());
		
		PowerMockito.doReturn(1.0).when(locacaoService, "calcularValorLocacao", filmes);
		
		//a��o
		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);
		
		//verifica��o
		Assert.assertThat(locacao.getValor(), is(1.0));
		
		PowerMockito.verifyPrivate(locacaoService).invoke("calcularValorLocacao", filmes);
	}
	
	@Test
	public void deveCalcularValorLocacao() throws Exception {
		//cenario
		List<Filme> filmes = Arrays.asList(umFilme().agora());
		
		//a��o
		Double valor = (Double) Whitebox.invokeMethod(locacaoService, "calcularValorLocacao", filmes);
		
		//verifica��o
		Assert.assertThat(valor, is(4.0));
	
	}
}
