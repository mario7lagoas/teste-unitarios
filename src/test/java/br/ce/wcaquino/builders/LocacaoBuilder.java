package br.ce.wcaquino.builders;

import static br.ce.wcaquino.builders.UsuarioBuilder.umUsuario;

import java.util.Arrays;
import java.util.Date;

import br.ce.wcaquino.entidades.Locacao;
import br.ce.wcaquino.entidades.Usuario;
import br.ce.wcaquino.utils.DataUtils;

public class LocacaoBuilder {
	
	private Locacao locacao;
	
	public LocacaoBuilder() {}
	
	public  static LocacaoBuilder umaLocacao() {
		LocacaoBuilder builder = new LocacaoBuilder();
		
		
		builder.locacao = new Locacao();
		
		builder.locacao.setUsuario(umUsuario().agora());
		builder.locacao.setFilme(Arrays.asList(FilmeBuilder.umFilme().agora()));
		builder.locacao.setValor(4.0);
		builder.locacao.setDataLocacao(new Date());
		builder.locacao.setDataRetorno(DataUtils.obterDataComDiferencaDias(1));
		
		return builder;
	}


	public LocacaoBuilder comDataRetorno(Date dias) {
		locacao.setDataRetorno(dias);
		return this;
	}
	
	public LocacaoBuilder comUsuario(Usuario usuario) {
		locacao.setUsuario(usuario);
		return this;
	}
	
	public  Locacao agora() {
		return locacao;
	}
	
	public LocacaoBuilder atrasado() {
		locacao.setDataLocacao(DataUtils.obterDataComDiferencaDias(-4));
		locacao.setDataRetorno(DataUtils.obterDataComDiferencaDias(-2));
		return this;
		
	}
	
	

}
